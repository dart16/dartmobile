class PointRound {
  int pointRound = 0;

  PointRound(this.pointRound);

  int get getpointRound => pointRound;
  set setpointRound(int pointRound) => pointRound;

  int RoundCount() {
    return pointRound++;
  }

  int showPointRound() {
    return pointRound;
  }

  @override
  String toString() {
    return ' $pointRound';
  }
}
