abstract class AbsCard {
  void StartGame();
  void InputName();
  void Matchpoint();
  void FirstCard();
  void Deal();
  void DealorPassPlayer1();
  void DealorPassPlayer2();
  void Specialcard();
  int RandomSpecialcardPlayer(int input1, int input2, int rng);

  void CheckTotalPoint();
  void Lastturn();
  void NewRound();
}
