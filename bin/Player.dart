class Player {
  String player = '';

  String get getPlayer => player;
  set setPlayer(String player) => player;

  @override
  String toString() {
    return '🧒🏽 NamePlayer : $player';
  }
}
